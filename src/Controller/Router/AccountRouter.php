<?php

namespace App\Controller\Router;

use App\Domain\Helper\RequestOptionsExtractor;
use App\Domain\Service\AccountCrudService;
use App\Shared\Helpers\RouterValuesFactory;
use App\Shared\Infrastructure\BaseRouter;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: "/account")]
class AccountRouter extends BaseRouter
{

    public function __construct(AccountCrudService $service, RequestOptionsExtractor $extractor)
    {
        parent::__construct(
            routes: RouterValuesFactory::produceRouterValuesFor("account"),
            service: $service,
            dataExtractor: $extractor
        );
    }

}
