<?php

namespace App\Controller\Api;

use App\Domain\Service\AccountCrudService;
use App\Shared\Helpers\RouterValuesFactory;
use App\Shared\Infrastructure\BaseApi;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: "/api/account")]
class AccountApi extends BaseApi
{
    public function __construct(AccountCrudService $service)
    {
        parent::__construct(
            routes: RouterValuesFactory::produceRouterValuesFor("account"),
            service: $service
        );
    }


}