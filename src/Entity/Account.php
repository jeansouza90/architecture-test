<?php

namespace App\Entity;

use App\Repository\AccountRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AccountRepository::class)
 */
class Account implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $ts_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $ts_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $ts_deleted;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTsCreated(): ?\DateTimeInterface
    {
        return $this->ts_created;
    }

    public function setTsCreated(\DateTimeInterface $ts_created): self
    {
        $this->ts_created = $ts_created;

        return $this;
    }

    public function getTsUpdated(): ?\DateTimeInterface
    {
        return $this->ts_updated;
    }

    public function setTsUpdated(?\DateTimeInterface $ts_updated): self
    {
        $this->ts_updated = $ts_updated;

        return $this;
    }

    public function getTsDeleted(): ?\DateTimeInterface
    {
        return $this->ts_deleted;
    }

    public function setTsDeleted(?\DateTimeInterface $ts_deleted): self
    {
        $this->ts_deleted = $ts_deleted;
        return $this;
    }


    public function jsonSerialize()
    {
        return [
            "id" => $this->getId(),
            "name" => $this->getName(),
            "createdAt" => $this->getTsCreated()->format(\DateTime::W3C)
        ];
    }
}
