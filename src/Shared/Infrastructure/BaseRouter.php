<?php

namespace App\Shared\Infrastructure;


use App\Shared\Infrastructure\Contract\CrudContract;
use App\Shared\Infrastructure\Contract\RequestOptionsExtractorContract;
use App\Shared\Infrastructure\Dto\RouterValues;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\Routing\Annotation\Route;

class BaseRouter extends AbstractController
{
    #[Route(path: "/", methods: ['GET'])]
    public function readAll(Request $request): Response
    {
        if($this->routes->isReadAllEnabled()){
            throw new ServiceUnavailableHttpException(message: "Route unavailable");
        }

        $filter = $this->dataExtractor->getFilter($request);
        $order = $this->dataExtractor->getOrder($request);
        $pageSize = $this->dataExtractor->getPageSize($request);
        $page = $this->dataExtractor->getPage($request);

        return $this->render( $this->routes->getReadAll(),[
            "elements" => $this->service->readAll($filter, $order, $page, $pageSize)
        ]);
    }

    #[Route(path: "/{id}", methods: ['GET'])]
    public function read($id): Response
    {
        if($this->routes->isReadOneEnabled()){
            throw new ServiceUnavailableHttpException(message: "Route unavailable");
        }

        return $this->render($this->routes->getReadOne(), [
            "element" => $this->service->readOne($id)
        ]);
    }

    #[Route(path: "/", methods: ['POST'])]
    public function save(Request $request, CoachyEntityFactoryContract $entityFactory): Response
    {
        $isUpdateAction = false;
        if($this->routes->isCreateEnabled()
            || ( $isUpdateAction = $this->routes->isUpdateRequest($request))
        ){
            throw new ServiceUnavailableHttpException(message: "Route unavailable");
        }

        $twigFileName = $isUpdateAction ? $this->routes->getCreate() : $this->routes->getUpdate();
        $value = $isUpdateAction ? $this->service->update($entityFactory->getIdFrom($request), $request) : $this->service->create($request);
        return $this->render($twigFileName, [ "element" => $value ]);
    }

    #[Route(path: "/{id}/remove", methods: ['POST'])]
    public function remove($id): Response
    {
        if( $this->routes->isDeleteEnabled() ){
            throw new ServiceUnavailableHttpException(message: "Route unavailable");
        }

        $this->service->delete($id);

        return $this->render($this->routes->getDelete(), []);
    }

    public function __construct(
        protected RouterValues $routes,
        protected CrudContract $service,
        protected RequestOptionsExtractorContract $dataExtractor
    ){}


}