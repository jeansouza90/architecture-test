<?php

namespace App\Shared\Infrastructure;

use App\Domain\Helper\RequestOptionsExtractor;
use App\Shared\Infrastructure\Contract\CrudContract;

use App\Shared\Infrastructure\Dto\CoachyFactoryResponse;
use App\Shared\Infrastructure\Dto\RouterValues;
use App\Shared\Infrastructure\Exception\InvalidRequestException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\Routing\Annotation\Route;

class   BaseApi extends AbstractController
{

    #[Route(methods: ['GET'])]
    public function readAll(Request $request, RequestOptionsExtractor $dataExtractor): Response
    {
        if ($this->routes->isReadAllEnabled()) {
            throw new ServiceUnavailableHttpException(message: "Route unavailable");
        }

        $filter = $dataExtractor->getFilter($request);
        $order = $dataExtractor->getOrder($request);
        $pageSize = $dataExtractor->getPageSize($request);
        $page = $dataExtractor->getPage($request);

        $values = $this->service->readAll($filter, $order, $page, $pageSize);

        $factory = new CoachyFactoryResponse(
            data: $values,
            status: Response::HTTP_OK,
            page: $page,
            size: count($values)
        );

        return $factory->getResponse();
    }

    #[Route(path: "/{id}", methods: ['GET'])]
    public function read($id, Request $request): Response
    {
        if ($this->routes->isReadOneEnabled()) {
            throw new ServiceUnavailableHttpException(message: "Route unavailable");
        }

        $value = $this->service->readOne($id);

        if (is_null($value)) {
            $factory = new CoachyFactoryResponse(
                data: ["message" => "Content not found"],
                status: Response::HTTP_NOT_FOUND,
                page: null
            );
            return $factory->getResponse();
        }

        $factory = new CoachyFactoryResponse(
            data: $value,
            status: Response::HTTP_ACCEPTED,
            page: null
        );

        return $factory->getResponse();
    }

    #[Route(methods: ['POST'])]
    public function save(Request $request): Response
    {
        if ($this->routes->isCreateEnabled()) {
            throw new ServiceUnavailableHttpException(message: "Route unavailable");
        }

        $factory = new CoachyFactoryResponse(
            data: $this->service->create($request),
            status: Response::HTTP_CREATED,
            page: null
        );

        return $factory->getResponse();
    }

    #[Route(path: "/{id}", methods: ['PUT'])]
    public function update($id, Request $request): Response
    {
        if ($this->routes->isUpdateEnabled()) {
            throw new ServiceUnavailableHttpException(message: "Route unavailable");
        }

        $factory = new CoachyFactoryResponse(
            data: $this->service->update($id, $request),
            status: Response::HTTP_ACCEPTED,
            page: null
        );

        return $factory->getResponse();
    }

    #[Route(path: "/{id}", methods: ['DELETE'])]
    public function delete($id, Request $request): Response
    {
        if ($this->routes->isDeleteEnabled()) {
            throw new ServiceUnavailableHttpException(message: "Route unavailable");
        }
        try {
            $this->service->delete($id);

            $factory = new CoachyFactoryResponse(
                data: null,
                status: Response::HTTP_NO_CONTENT,
                page: null
            );

            return $factory->getResponse();
        } catch (InvalidRequestException $exception) {
            return CoachyFactoryResponse::fromError($exception, Response::HTTP_NOT_FOUND)->getResponse();
        }
    }

    public function __construct(
        protected RouterValues $routes,
        protected CrudContract $service
    )
    {
    }

}