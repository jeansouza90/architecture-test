<?php

namespace App\Shared\Infrastructure\Service;


use App\Shared\Infrastructure\BaseRepository;
use App\Shared\Infrastructure\Contract\CoachyEntityFactoryContract;
use App\Shared\Infrastructure\Contract\CrudContract;

use App\Shared\Infrastructure\Contract\RequestOptionsExtractorContract;
use App\Shared\Infrastructure\Contract\RequestValidator;
use App\Shared\Infrastructure\Exception\InvalidRequestException;
use App\Shared\Infrastructure\Exception\SearchRepositoryException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;

abstract class BaseCrudService implements CrudContract
{

    public function __construct(
        protected BaseRepository                  $repository,
        protected RequestOptionsExtractorContract $dataExtractor,
        protected CoachyEntityFactoryContract     $entityFactory,
        protected ?RequestValidator               $validator = null
    )
    {
    }

    public function readAll($filter = [], $order = [], $page = 1, $pageSize = 10): array
    {
        try {
            $values = $this->repository->findBy(
                $filter,
                $order,
                $pageSize,
                ($page - 1) * $pageSize
            );
            return $values;
        } catch (\Exception $exception) {
            throw new SearchRepositoryException("Error on reading page", $page, $pageSize, $filter, $order);
        }
    }

    public function readOne($id): ?object
    {
        try {
            return $this->repository->find($id);
        } catch (\Exception $exception) {
            throw new SearchRepositoryException("Error on reading value", $id, $exception);
        }
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function create(Request $request): ?object
    {
        if (!is_null($this->validator)) {
            $this->validator->validateSaveRequest($request);
        }

        $obj = $this->entityFactory->produceEntityByRequest($request);
        $this->repository->create($obj);

        return $obj;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function update($id, Request $request): ?object
    {
        if (!is_null($this->validator)) {
            $this->validator->validateSaveRequest($request);
        }

        $obj = $this->entityFactory->produceEntityByRequest($request);

        return $this->repository->update($id, $obj);
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function delete($id): void
    {
        $entity = $this->repository->find($id);

        if (is_null($entity)) {
            throw new InvalidRequestException("Value not found on database");
        }

        $this->repository->remove($id);
    }

}