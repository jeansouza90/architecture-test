<?php

namespace App\Shared\Infrastructure\Service\Exception;

use http\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\Response;

class InvalidSearchException extends RuntimeException
{
    public function __construct($message = "Invalid search", $code = Response::HTTP_BAD_REQUEST)
    {
        parent::__construct($message, $code, null);
    }


}