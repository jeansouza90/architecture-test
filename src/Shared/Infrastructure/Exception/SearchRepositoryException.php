<?php

namespace App\Shared\Infrastructure\Exception;

class SearchRepositoryException extends \RuntimeException
{

    public function __construct(string $message, $id, $exception)
    {
        parent::__construct( $message, $id, $exception);
    }


}