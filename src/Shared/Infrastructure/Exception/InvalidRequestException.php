<?php

namespace App\Shared\Infrastructure\Exception;

use Throwable;

class InvalidRequestException extends \RuntimeException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function __toString()
    {
        parent::__toString();
    }

    public function __wakeup()
    {
        parent::__wakeup();
    }


}