<?php

namespace App\Shared\Infrastructure\Dto;

use Symfony\Component\HttpFoundation\Request;

class RouterValues
{
    public function __construct(
        private $create,
        private $readOne,
        private $readAll,
        private $update,
        private $delete
    )
    {}

    public function getCreate(): ?string
    {
        return $this->create;
    }

    public function getReadOne(): ?string
    {
        return $this->readOne;
    }

    public function getReadAll(): ?string
    {
        return $this->readAll;
    }

    public function getUpdate(): ?string
    {
        return $this->update;
    }

    public function getDelete(): ?string
    {
        return $this->delete;
    }

    public function getMyRoute(): ?string
    {
        return $this->myRoute;
    }

    public function isCreateEnabled():bool
    {
        return empty($this->create);
    }

    public function isReadAllEnabled():bool
    {
        return empty($this->readAll);
    }

    public function isReadOneEnabled(): bool
    {
        return empty($this->readOne);
    }

    public function isUpdateEnabled(): bool
    {
        return empty($this->update);
    }

    public function isDeleteEnabled(): bool
    {
        return empty($this->delete);
    }

    public function isUpdateRequest(Request $request) : bool
    {
        return false;
    }


}