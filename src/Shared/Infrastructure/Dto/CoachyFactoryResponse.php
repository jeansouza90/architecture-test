<?php

namespace App\Shared\Infrastructure\Dto;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class CoachyFactoryResponse
{

    public function __construct(
            private      $data,
            private ?int $status = Response::HTTP_OK,
            private ?int $page = 1,
            private ?int $size = 20){}

    public static function fromError(\Throwable $error, $status = Response::HTTP_INTERNAL_SERVER_ERROR) : CoachyFactoryResponse
    {
        return new self(['message' => $error->getMessage()],$status, null);
    }

    public function getResponse(): JsonResponse
    {
        if(is_null($this->data)){
            return new JsonResponse(null, $this->status);
        }

        $data = [
            "data" => $this->data,
            "page" => $this->page,
            "size" => $this->size
        ];

        if (is_null($this->page)) {
            unset($data['page']);
            unset($data['size']);
        }

        return new JsonResponse($data, $this->status);
    }
}