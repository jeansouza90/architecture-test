<?php

namespace App\Shared\Infrastructure\Contract;

use Symfony\Component\HttpFoundation\Request;

interface CrudContract
{
    public function create(Request $request): ?object;

    public function readAll($filter = [], $order = [], $page = 1, $pageSize = 10): array;

    public function readOne($id): ?object;

    public function update($id, Request $request): ?object;

    public function delete($id): void;

}