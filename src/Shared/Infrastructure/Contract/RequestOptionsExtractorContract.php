<?php

namespace App\Shared\Infrastructure\Contract;

use Symfony\Component\HttpFoundation\Request;

interface RequestOptionsExtractorContract
{

    public function getPage(Request $request): int;

    public function getPageSize(Request $request): int;

    public function getFilter(Request $request) : array;

    public function getOrder(Request $request): array;


}