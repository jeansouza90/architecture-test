<?php

namespace App\Shared\Infrastructure\Contract;

use Symfony\Component\HttpFoundation\Request;

interface CoachyEntityFactoryContract
{

    public function produceEntityByRequest(Request $request);

    public function getIdFrom(Request $request);
}