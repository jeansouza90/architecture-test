<?php

namespace App\Shared\Infrastructure\Contract;

use Symfony\Component\HttpFoundation\Request;

interface RequestValidator
{
    /**
     * @throws InvalidRequestException
     */
    function validateSaveRequest(Request $request): void;

}