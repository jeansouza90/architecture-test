<?php

namespace App\Shared\Infrastructure;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

abstract class BaseRepository extends ServiceEntityRepository
{

    public function __construct(
        ManagerRegistry $registry,
        string $entityClass
    ){
        parent::__construct($registry, $entityClass);
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function create($obj)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($obj);
        $entityManager->flush();

        return $obj;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function update($id, $entityFromRequest)
    {
        $entityManager = $this->getEntityManager();
        $entity = $this->find($id);
        $this->updateExistingEntityWith($entity, $entityFromRequest);

        $entityManager->flush();

        return $entity;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function remove($id): void
    {
        $entityManager = $this->getEntityManager();

        $entity = $this->find($id);
        $entityManager->remove($entity);
        $entityManager->flush();
    }

    protected abstract function updateExistingEntityWith($entity, $entityFromRequest): void;

}