<?php

namespace App\Shared\Helpers;



use App\Shared\Infrastructure\Dto\RouterValues;

class RouterValuesFactory
{
    public static function produceRouterValuesFor($domain) : RouterValues
    {
        return new RouterValues(
            create: "$domain/$domain-form.html.twig",
            readOne: "$domain/$domain-detail.html.twig",
            readAll: "$domain/$domain-list.html.twig",
            update: "$domain/$domain-form.html.twig",
            delete: "$domain/$domain-list.html.twig"
        );
    }

}