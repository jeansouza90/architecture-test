<?php

namespace App\Domain\Validator;

use App\Domain\Service\EntityConvertorFactory\AccountEntityFactory;
use App\Shared\Infrastructure\Contract\RequestValidator;
use http\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\Request;

class AccountRequestValidator implements RequestValidator
{
    public function __construct(
        private AccountEntityFactory $entityFactory
    ){}

    function validateSaveRequest(Request $request): void
    {
        $account = $this->entityFactory->produceEntityByRequest($request);
        if(empty($account->getName())){
            throw new RuntimeException("Account should have name");
        }
    }


}