<?php

namespace App\Domain\Service;

use App\Domain\Helper\RequestOptionsExtractor;
use App\Domain\Service\EntityConvertorFactory\AccountEntityFactory;
use App\Domain\Validator\AccountRequestValidator;
use App\Repository\AccountRepository;
use App\Shared\Infrastructure\Service\BaseCrudService;

class AccountCrudService extends BaseCrudService
{
    public function __construct(AccountRepository       $repository,
                                RequestOptionsExtractor $dataExtractor,
                                AccountEntityFactory    $entityFactory,
                                AccountRequestValidator $validator
    )
    {
        parent::__construct($repository, $dataExtractor, $entityFactory, $validator);
    }



}