<?php

namespace App\Domain\Service\EntityConvertorFactory;


use App\Entity\Account;
use App\Shared\Infrastructure\Contract\CoachyEntityFactoryContract;
use Symfony\Component\HttpFoundation\Request;

class AccountEntityFactory implements CoachyEntityFactoryContract
{

    public function produceEntityByRequest(Request $request): Account
    {
        $jsonObject = json_decode($request->getContent());
        $account = new Account();

        if(isset($jsonObject->id)){
            $account->setId($jsonObject->id);
        }

        $account->setName($jsonObject->name);
        $account->setTsCreated(new \DateTime());

        return $account;

    }

    public function getIdFrom(Request $request)
    {
        return $this->produceEntityByRequest($request)->getId();
    }
}