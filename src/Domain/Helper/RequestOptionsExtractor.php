<?php

namespace App\Domain\Helper;


use App\Shared\Infrastructure\Contract\RequestOptionsExtractorContract;
use Symfony\Component\HttpFoundation\Request;

class RequestOptionsExtractor implements RequestOptionsExtractorContract
{

    public function getPage(Request $request): int
    {
        [ , , $page, ] = $this->getUrlData($request);
        return $page;
    }

    public function getPageSize(Request $request): int
    {
        [ , , , $size ] = $this->getUrlData($request);
        return $size;
    }

    public function getFilter(Request $request): array
    {
        [$filterData, , ] = $this->getUrlData($request);
        return $filterData;
    }

    public function getOrder(Request $request): array
    {
        [, $orderData, ] = $this->getUrlData($request);
        return $orderData;
    }

    private function getUrlData(Request $request): array
    {
        $queryData = $request->query->all();

        $orderData = array_key_exists('sort', $queryData) ? $queryData['sort'] : [];
        $page = array_key_exists('page', $queryData) ? $queryData['page'] : 1;
        $pageSize = array_key_exists('size', $queryData) ? $queryData['size'] : 10;

        unset($queryData['sort']);
        unset($queryData['page']);
        unset($queryData['size']);

        return [$queryData, $orderData, $page, $pageSize];
    }
}