<?php

namespace App\Repository;

use App\Entity\Account;
use App\Shared\Infrastructure\BaseRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Account|null find($id, $lockMode = null, $lockVersion = null)
 * @method Account|null findOneBy(array $criteria, array $orderBy = null)
 * @method Account[]    findAll()
 * @method Account[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Account::class);
    }

    /**
     * @param Account $entity
     * @param Account $entityFromRequest
     */
    protected function updateExistingEntityWith($entity, $entityFromRequest): void
    {
        $entity->setTsUpdated(new \DateTime());
        $entity->setName($entityFromRequest->getName());
    }


}
